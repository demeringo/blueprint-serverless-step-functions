# Blueprint Serverless Step Functions

A simple project to show usage of AWS step functions with the Serverless  framework (https://www.serverless.com/) 

## Content

## Usage

### Setup

Install nodejs 14 (see https://github.com/nodesource/distributions/blob/master/README.md)

```sh
# Using Debian, as root
curl -fsSL https://deb.nodesource.com/setup_14.x | bash -
apt-get install -y nodejs
```

Create new sls node project (**not** needed if you clone this repo) `sls create -t aws-nodejs`

Note also that we add _serverless_ package as a dev dependency (so that we can pin version used).

```sh
npm -i save-dev serverless
```

### Setting Up AWS and Gitlab CI

1. Create AWS credentials including the following inline IAM policy:
    ```json
    {
        "Statement": [
            {
                "Action": [
                    "apigateway:*",
                    "cloudformation:CancelUpdateStack",
                    "cloudformation:ContinueUpdateRollback",
                    "cloudformation:CreateChangeSet",
                    "cloudformation:CreateStack",
                    "cloudformation:CreateUploadBucket",
                    "cloudformation:DeleteStack",
                    "cloudformation:Describe*",
                    "cloudformation:EstimateTemplateCost",
                    "cloudformation:ExecuteChangeSet",
                    "cloudformation:Get*",
                    "cloudformation:List*",
                    "cloudformation:PreviewStackUpdate",
                    "cloudformation:UpdateStack",
                    "cloudformation:UpdateTerminationProtection",
                    "cloudformation:ValidateTemplate",
                    "dynamodb:CreateTable",
                    "dynamodb:DeleteTable",
                    "dynamodb:DescribeTable",
                    "ec2:AttachInternetGateway",
                    "ec2:AuthorizeSecurityGroupIngress",
                    "ec2:CreateInternetGateway",
                    "ec2:CreateNetworkAcl",
                    "ec2:CreateNetworkAclEntry",
                    "ec2:CreateRouteTable",
                    "ec2:CreateSecurityGroup",
                    "ec2:CreateSubnet",
                    "ec2:CreateTags",
                    "ec2:CreateVpc",
                    "ec2:DeleteInternetGateway",
                    "ec2:DeleteNetworkAcl",
                    "ec2:DeleteNetworkAclEntry",
                    "ec2:DeleteRouteTable",
                    "ec2:DeleteSecurityGroup",
                    "ec2:DeleteSubnet",
                    "ec2:DeleteVpc",
                    "ec2:Describe*",
                    "ec2:DetachInternetGateway",
                    "ec2:ModifyVpcAttribute",
                    "events:DeleteRule",
                    "events:DescribeRule",
                    "events:ListRuleNamesByTarget",
                    "events:ListRules",
                    "events:ListTargetsByRule",
                    "events:PutRule",
                    "events:PutTargets",
                    "events:RemoveTargets",
                    "iam:CreateRole",
                    "iam:DeleteRole",
                    "iam:DeleteRolePolicy",
                    "iam:GetRole",
                    "iam:PassRole",
                    "iam:PutRolePolicy",
                    "iot:CreateTopicRule",
                    "iot:DeleteTopicRule",
                    "iot:DisableTopicRule",
                    "iot:EnableTopicRule",
                    "iot:ReplaceTopicRule",
                    "kinesis:CreateStream",
                    "kinesis:DeleteStream",
                    "kinesis:DescribeStream",
                    "lambda:*",
                    "logs:CreateLogGroup",
                    "logs:DeleteLogGroup",
                    "logs:DescribeLogGroups",
                    "logs:DescribeLogStreams",
                    "logs:FilterLogEvents",
                    "logs:GetLogEvents",
                    "s3:CreateBucket",
                    "s3:DeleteBucket",
                    "s3:DeleteBucketPolicy",
                    "s3:DeleteObject",
                    "s3:DeleteObjectVersion",
                    "s3:GetObject",
                    "s3:GetObjectVersion",
                    "s3:ListAllMyBuckets",
                    "s3:ListBucket",
                    "s3:PutBucketNotification",
                    "s3:PutBucketPolicy",
                    "s3:PutBucketTagging",
                    "s3:PutBucketWebsite",
                    "s3:PutEncryptionConfiguration",
                    "s3:PutObject",
                    "sns:CreateTopic",
                    "sns:DeleteTopic",
                    "sns:GetSubscriptionAttributes",
                    "sns:GetTopicAttributes",
                    "sns:ListSubscriptions",
                    "sns:ListSubscriptionsByTopic",
                    "sns:ListTopics",
                    "sns:SetSubscriptionAttributes",
                    "sns:SetTopicAttributes",
                    "sns:Subscribe",
                    "sns:Unsubscribe",
                    "states:CreateStateMachine",
                    "states:DeleteStateMachine"
                ],
                "Effect": "Allow",
                "Resource": "*"
            }
        ],
        "Version": "2012-10-17"
    }
    ```

1. Set the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` variables in the GitLab CI/CD settings. `Settings > CI/CD > Variables`.

_When testing deployment from local cli, you can set a profile with `export AWS_PROFILE=name-of-profile`__
